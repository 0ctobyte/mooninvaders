# Mooninvaders

Mooninvaders. Final project for a high school computer science course (grade 11). Completed around June, 2009.

## Build
- `make`
- `make install`

To run:
`java -jar mooninvaders.jar`

